C++Utils
--------

## Usage

## Methods

* **Math**

       * [**compute(const string &, int &)**](#markdown-header-computeconst-string-int)

----

*   **String**

       * [**split(const string&, char)**](#markdown-header-split-const-string-char)
       * [**split(const string&, const string&)**](#markdown-header-split-const-string-const-string)
       * [**strip(const string&, char)**](#markdown-header-strip-const-string-char-bool)
       * [**strip(const string&, const string&)**](#markdown-header-strip-const-string-constr-string)
       * [**lstrip ( const string &, char, bool )**](#markdown-header-lstrip-const-string-char-bool)
       * [**lstrip ( const string &, const string & )**](#markdown-header-lstrip-const-string-const-string)
       * [**rstrip ( const string &, char, bool )**](#markdown-header-rstrip-const-string-char-bool)
       * [**rstrip ( const string &, const string & )**](#markdown-header-rstrip-const-string-const-string)
       * [**repeat ( const string &, unsigned int, char, bool )**](#markdown-header-repeat-const-string-unsigned-int-char-bool)
       * [**repeat ( const string &, unsigned int, const string& )**](#markdown-header-repeat-const-string-unsigned-int-const-string)
       * [**normalize( const string & )**](#markdown-header-normalize-const-string)
       * [**unique( const string &)**](#markdown-header-unique-const-string)
       * [**count ( const string &, char )**](#markdown-header-count-const-string-char)
       * [**count ( const string &, const string & )**](#markdown-header-count-const-string-const-string)
       * [**occurence ( const string &, char, long int )**](#markdown-header-occurence-const-string-char-long-int)
       * [**occurence ( const string &, const string&, long int )**](#markdown-header-occurence-const-string-const-string-long-int)
       * [**indecies ( const string &, char )**](#markdown-header-indecies-const-string-char)
       * [**indecies ( const string &, const string&, bool )**](#markdown-header-indecies-const-string-const-string-bool)
       * [**between(const string&, char, unsigned int, unsigned int)**](#markdown-header-between-const-string-char-unsigned-int-unsigned-int)
       * [**between(const string&, const string&, unsigned int, unsigned int)**](#markdown-header-between-const-string-const-string-unsigned-int-unsigned-int)
       * [**in(const string&, char)**](#markdown-header-in-const-string-char)
       * [**in(const string&, const string&)**](#markdown-header-in-const-string-const-string)

----

### **compute(const string &, int &);**

Computes a string expression, and keeps the result in an int object.

`@args`

    const string & expr

        Math expr, given as string.


    int & result

        Assigns the result to that arg.

`@throws`

    An error, if you try to divide by 0.

### **split ( const string &, char = ' ' );**

Splits a long string into chunks, depending from the given delimeter.
By default delim is space.

`@args`

    const string & text

        Text, which will be divided into chunks

    char delim

        Char, by which the text will be divided.

`@return`

    vector<string>

        Collection with string objects.


### **split ( const string &, const string & );**

Splits a long string into chunks, depending from the given word/text.

`@args`

    const string & text

        Text, which will be divided into chunks

    const string & word

        Word/Text, by which the text will be divided.

`@return`

    vector<string>

        Collection with string objects.

### **strip ( const string &, char, bool );**

Removes all whitespaces, newline characters and tabs or specified char from the beginning and ending of the string.

`@args`

    const string & str

        String which will be manipulated.

    char c

        Specifies which char to be removed from the beginning, and ending of the given string.

    bool by_default

        Because `c` cannot be an empty char, by default is set to whitespace, so if you want only
        to remove only whitespaces, without newlines, or tabs, set that to false.

`@return`

    string result

        Result of the function


### **strip ( const string &, const string & );**

Removes all occurences of the given substr from the beginning and ending of the string.

`@args`

    const string & str

        String which will be manipulated.

    const string & substr

        String which will be searched for in the given str.

`@return`

    string result

        Result of the function

### **lstrip ( const string &, char, bool );**

Removes all whitespaces, newline characters and tabs or specified char from the beginning  of the string.

`@args`

    const string & str

        String which will be manipulated.

    char c

        Specifies which char to be removed from the beginning of the given string.

    bool by_default

        Because `c` cannot be an empty char, by default is set to whitespace, so if you want only to remove only whitespaces, without newlines, or tabs, set that to false.

`@return`
    string result
        Result of the function

### **lstrip ( const string &, const string & );**

Removes all occurenses of the specified substr from the beginning of the string.

`@args`

    const string & str

        String which will be manipulated.

    const string & substr

        String which will be searched for from the beginning.

`@return`

    string result

        Result of the function

### **rstrip ( const string &, char, bool );**

Removes all whitespaces, newline characters and tabs or specified char from the end  of the string.

`@args`

    const string & str

        String which will be manipulated.

    char c

        Specifies which char to be removed from the end of the given string.

    bool by_default

        Because `c` cannot be an empty char, by default is set to whitespace, so if you want only to remove only whitespaces, without newlines, or tabs, set that to false.

`@return`

    string result

        Result of the function


### **rstrip ( const string &, char, bool );**

Removes all occurenses of the specified substr from the beginning of the string.

`@args`

    const string & str

        String which will be manipulated.

    const string & substr

        String which will be searched for from the end.

`@return`

    string result

        Result of the function

### **repeat ( const string &, unsigned int, char, bool );**

Repeats a string number of times.

`@args`

    const string & str

        String to be repeated

    unsigned int time

        Number of times to be repeated

    char delim

        Delimeter to be set between repeatings.

    bool set

        By default is set to false, which means that delim will not be take into consideration.

`@return`

    string result

        Result of the operation.

### **repeat ( const string &, unsigned int, const string & );**

Repeats a string number of times.

`@args`

    const string & str

        String to be repeated

    unsigned int time

        Number of times to be repeated

    const string & other

        Insert another string between repeatings of the string.

`@return`

    string result

        Result of the operation.

### **normalize( const string & );**

Normalizes a string, removes unnecesary whitespaces, capitalizes all words after punctuations.

`@args`

    const string & str

        A string to be normalized.

`@return`

    string result

        Result of the operation.


### **unique( const string & );**

Returns all unique words in a string. It doesn't change capitalization of a string.

`@args`

    const string & str

        String to be modified.

`@return`

    string result

        Result of the operation.

### **count ( const string &, char );**

Counts the number of occurences of a given char in a given string.

`@args`

    const string & str

        String in which a given char will be checked.

    char c

        Char to be searched for in a string.

`@return`

    unsigned int total

        Number of occurences of the char in the given string.

### **count ( const string &, const string& );**

Counts the number of occurences of a given substr in a given string.

`@args`

    const string & str

        String in which a given char will be checked.

    const string & substr

        Substr to be searched for in a string.

`@return`

    unsigned int total

        Number of occurences of the char in the given string.

### **occurence ( const string &, char, long int );**

Searches for a given char from the beginning or ending of a string, 
depending from the given index.

`@args`

    const string & str

        String to be searched in.

    char c

        Char to be searched for.

    long int index

        From where the search to happen. If index is negative, it will search from index to the beginnning,
        if index is positive it will search from index to the end of the string.

`@return`

    long int index

        The first index in which the given char is found, or -1 if it's not.

### **occurence ( const string &, char, long int );**

Searches for a given substr from the beginning or ending of a string, depending from the given index.

`@args`

    const string & str

        String to be searched in.

    const string & substr

        Substr to be searched for.

    long int index

        From where the search to happen. If index is negative, it will search from index to the beginnning,
        if index is positive it will search from index to the end of the string.

`@return`

    long int index

        The first index in which the given char is found, or -1 if it's not.

### **indecies ( const string &, char )**

Finds all indecies of all occurences of a given char.

`@args`

    const string & str

        String to be used for.

    char c

        Char to be searched for.

`@return`

    vector<unsigned int> index

        All indecies containing the char at the given string.

### **indecies ( const string &, const string, bool )**

Finds all indecies of all occurences of a given substr.

`@args`

    const string & str

        String to be used for.

    const string & substr

        String to be searched for.

    bool exact

        Default: false. Means that if set to true, no overlaping indecies will be included 
        in the result.

`@return`

    vector<unsigned int> index

        All indecies containing the char at the given string.


### **between( const string&, char, unsigned int, unsigned in)**

Checks if a given char is contained in a string between [begin, end) positions.


`@args`

    const string & str

        String to be checked.

    char c

        Char to be searched for in the given string.

    unsigned int begin

        Index from which the search to begin.

    unsigned int end

        Index to which search to end.

`@return`

    true, or false

### **between( const string&, const string&, unsigned int, unsigned in)**

Checks if a given string is contained in a string between [begin, end) positions.

`@args`

    const string & str

        String to be checked.

    const string & substr

        String to be searched for.

    unsigned int begin

        Index from which the search to begin.

    unsigned int end

        Index to which search to end.

`@return`

    true, or false


### **in ( const string &, char );**

Checks if a given char is contained in the string.

`@args`

    const string & str

        String which will be checked.

    char c

        Char which will be checked.

`@return`

    true, or false

### **in ( const string &, const string & );**

Checks if a given char is contained in the string.

`@args`

    const string & str

        String which will be checked.

    const string & other

        String to be checked.

`@return`

    true, or false


## License

**MIT**