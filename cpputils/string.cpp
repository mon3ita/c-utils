#include "string.h"

using namespace str;

/*
Splits a long string into chunks, depending from the given delimeter.
By default delim is space.

@args
    const string & text
        Text, which will be divided into chunks
    char delim
        Char, by which the text will be divided.
@return
    vector<string>
        Collection with string objects.
*/
vector<string> str::split(const string & text, char delim) {
    vector<string> result;

    string current_string;
    unsigned int i = 0;
    for(;i < text.size(); i++) {
        if(text[i] != delim)
            current_string.push_back(text[i]);
        else {
            result.push_back(current_string);
            current_string.clear();
        }
    }

    if(text[i - 1] != ' ')
        result.push_back(current_string);

    return result;
}

/*
Splits a long string into chunks, depending from the given word/text.

@args
    const string & text
        Text, which will be divided into chunks
    const string & word
        Word/Text, by which the text will be divided.
@return
    vector<string>
        Collection with string objects.
*/
vector<string> str::split(const string & text, const string & word) {
    vector<string> result;

    string current_string;

    unsigned int i = 0;
    for(;i < text.size(); i++) {
        if(text.substr(i, word.size()) == word) {
            result.push_back(current_string);
            current_string.clear();
            i += word.size();
        } else 
            current_string.push_back(text[i]);
    }

    if(text.size() > word.size() && text.substr(i - word.size() - 1, word.size()) != word)
        result.push_back(current_string);

    return result;
}

/*
Removes all whitespaces, newline characters and tabs or specified char from the beginning and ending of the string.

@args
    const string & str
        String which will be manipulated.
    char c
        Specifies which char to be removed from the beginning, and ending of the given string.
    bool by_default
        Because `c` cannot be an empty char, by default is set to whitespace, so if you want only
        to remove only whitespaces, without newlines, or tabs, set that to false.
@return
    string result
        Result of the function
*/
string str::strip(const string & str, char c, bool by_default) {
    string result;

    if(c != ' ' && by_default) by_default = false;

    unsigned int i, j;

    i = 0;
    j = str.size() - 1;
    if(by_default) {
        while(str[i] == ' ' || str[i] == '\n' || str[i] == '\t') ++i;
        while(str[j] == ' ' || str[j] == '\n' || str[j] == '\t') --j;
    } else {
        while(str[i++] == c) ;
        while(str[j--] == c) ;
    }

    result = str.substr(i, j);
    return result;
}

/*
Removes all occurences of the given substr from the beginning and ending of the string.

@args
    const string & str
        String which will be manipulated.
    const string & substr
        String which will be searched for in the given str.

@return
    string result
        Result of the function
*/
string str::strip(const string & str, const string & substr) {
    string result;

    unsigned int i = 0, j = str.size() - substr.size();
    int count = 0;
    while(i <= j && count < str.size()) {
        if(str.substr(i, substr.size()) == substr)
            i += substr.size();

        if(str.substr(j, substr.size()) == substr)
            j -= substr.size();

        if(i > 0 || j < str.size() - 1)
            count += substr.size();
        else
            ++count;
    }

    result = str.substr(i, j);
    return result;
}

/*
Removes all whitespaces, newline characters and tabs or specified char from the beginning of the string.

@args
    const string & str
        String which will be manipulated.
    char c
        Specifies which char to be removed from the beginning of the given string.
    bool by_default
        Because `c` cannot be an empty char, by default is set to whitespace, so if you want only
        to remove only whitespaces, without newlines, or tabs, set that to false.
@return
    string result
        Result of the function
*/
string str::lstrip(const string & str, char c, bool by_default) {
    string result;

    if(c != ' ' && by_default) by_default = false;

    unsigned int i = 0;

    if(by_default)
        while(str[i] == ' ' || str[i] == '\n' || str[i] == '\t') ++i;
    else
        while(str[i++] == c) ;


    result = str.substr(i, str.size());
    return result;
}

/*
Removes all occurenses of the specified substr from the beginning of the string.

@args
    const string & str
        String which will be manipulated.
    const string & substr
        String which will be searched for from the beginning.
@return
    string result
        Result of the function
*/
string str::lstrip(const string & str, const string & substr) {
    string result;

    unsigned int i = 0;
    int count = 0;
    while(count < str.size()) {
        if(str.substr(i, substr.size()) == substr)
            i += substr.size();

        count += i > 0 ? substr.size() : 1;
    }

    result = str.substr(i, str.size());
    return result;
}

/*
Removes all whitespaces, newline characters and tabs or specified char from the end of the string.

@args
    const string & str
        String which will be manipulated.
    char c
        Specifies which char to be removed from the end of the given string.
    bool by_default
        Because `c` cannot be an empty char, by default is set to whitespace, so if you want only
        to remove only whitespaces, without newlines, or tabs, set that to false.
@return
    string result
        Result of the function
*/
string str::rstrip(const string & str, char c, bool by_default) {
    string result;

    if(c != ' ' && by_default) by_default = false;

    unsigned int i = str.size() - 2;

    if(by_default)
        while(str[i] == ' ' || str[i] == '\n' || str[i] == '\t') --i;
    else
        while(str[i--] == c) ;


    i += 2;
    result = str.substr(0, i);
    return result;
}

/*
Removes all occurenses of the specified substr from the end of the string.

@args
    const string & str
        String which will be manipulated.
    const string & substr
        String which will be searched for from the beginning.
@return
    string result
        Result of the function
*/
string str::rstrip(const string & str, const string & substr) {
    string result;

    unsigned int i = str.size() - substr.size();
    bool changed = false;
    int count = 0;
    while(count < str.size()) {
        if(str.substr(i, substr.size()) == substr) {
            i -= substr.size();
            changed = true;
        }

        count += changed ? substr.size() : 1;
    }

    i += substr.size();
    result = str.substr(0, i);
    return result;
}

/*
Repeats a string number of times.

@args
    const string & str
        String to be repeated
    unsigned int time
        Number of times to be repeated
    char delim
        Delimeter to be set between repeatings.
    bool set
        By default is set to false, which means that delim will not be take into consideration.

@return
    string result
        Result of the operation.

*/
string str::repeat(const string& str, unsigned int times, char delim, bool set) {
    string result;
    if(!set)
        do {
            result += str;
        } while(times-- > 0);
    else
        do {
            result += str + delim;
        } while(times-- > 0);
    return result;
}

/*
Repeats a string number of times.

@args
    const string & str
        String to be repeated
    unsigned int time
        Number of times to be repeated
    const string & other
        Insert another string between repeatings of the string.

@return
    string result
        Result of the operation.

*/
string str::repeat(const string& str, unsigned int times, const string & other) {
    string result;
    do {
        result += str + other;
    } while(times-- > 0);

    result = result.substr(0, result.size() - other.size());

    return result;
}

/*
Normalizes a string, removes unnecesary whitespaces, capitalizes all words after punctuations.

@args
    const string & str
        A string to be normalized.

@return
    string result
        Result of the operation.

*/
string str::normalize(const string & str) {
    string result;

    bool whitespaces = false;
    unsigned int j = 0, previous = 0;
    for(unsigned int i = 0; i < str.size(); ++i, ++j) {
        if(!result.size() && i >= 0 && (str[i] >= 'a' && (str[i]  <= 'z' || str[i] <= 'Z'))) {
            result.push_back('\t');
            result.push_back(::toupper(str[i]));
        }

        while(str[j] == ' ') ++j;
        if(j == i + 1) ;
        else {
            previous = i;
            i = j;
        }

        if(i > 0 && in(".!?", str[previous - 1])) {
            result.push_back(' ');
            result.push_back(::toupper(str[i]));
        }
        else
            result.push_back(::tolower(str[i]));

        previous = i;
    }

    return result;
}

/*
Returns all unique words in a string. It doesn't change capitalization of a string.

@args
    const string & str
        String to be modified.

@return
    string result
        Result of the operation.
*/
string str::unique( const string & str ) {
    string result;
    vector<string> splitted = split(str);
    map<string, bool> used;

    for(auto word : splitted) used[word] = 0;

    for(auto word : splitted) {
        if(!used[word]) result += word + ' ';
        used[word] = true;
    }

    return result.substr(0, result.size() - 1);
}

/*
Counts the number of occurences of a given char in a given string.

@args
    const string & str
        String in which a given char will be checked.
    char c
        Char to be searched for in a string.
@return
    unsigned int total
        Number of occurences of the char in the given string.
*/
unsigned int str::count(const string & str, char c) {
    unsigned int total = 0;
    for(unsigned int i = 0; i < str.size(); ++i)
        if(str[i] == c) ++total;
    return total;
}

/*
Counts the number of occurences of a given substr in a given string.

@args
    const string & str
        String in which a given char will be checked.
    const string & str
        String to be searched for in a string.
@return
    unsigned int total
        Number of occurences of the char in the given string.
*/
unsigned int str::count(const string & str, const string & substr) {
    unsigned int total = 0;
    for(unsigned int i = 0; i < str.size(); ++i)
        if(str.substr(i, substr.size()) == substr) ++total;
    return total;
}

/*
Searches for a given char from the beginning or ending of a string, depending from the given index.

@args
    const string & str
        String to be searched in.
    char c
        Char to be searched for.
    long int index
        From where the search to happen. If index is negative, it will search from index to the beginnning,
        if index is positive it will search from index to the end of the string.

@return
    long int index
        The first index in which the given char is found, or -1 if it's not.

*/
long int str::occurence(const string& str, char c, long int index) {
    long int i = index < 0 ? str.size() + index : 0;

    if(index < 0)
        while(str[i--] != c && i >= 0) ;
    else
        while(str[i++] != c && i < str.size()) ;

    i = str[i + 1] == c ? i + 1 : -1;
    return i;
}


/*
Searches for a given substr from the beginning or ending of a string, depending from the given index.

@args
    const string & str
        String to be searched in.
    const string & substr
        Substr to be searched for.
    long int index
        From where the search to happen. If index is negative, it will search from index to the beginnning,
        if index is positive it will search from index to the end of the string.

@return
    long int index
        The first index in which the given char is found, or -1 if it's not.

*/
long int str::occurence(const string& str, const string& substr, long int index) {
    if(str.size() - substr.size() + index < 0)
        throw "Out of range";

    long int i = index < 0 ? str.size() + index : 0;

    if(index < 0)
        while(str.substr(i, substr.size()) != substr && i >= 0) --i;
    else
        while(str.substr(i, substr.size()) != substr && i < (str.size() - substr.size())) ++i;

    i = i < 0 || i >= str.size() + substr.size() ? -1 : i;
    return i;
}

/*
Finds all indecies of all occurences of a given char.

@args
    const string & str
        String to be used for.
    char c
        Char to be searched for.

@return
    vector<unsigned int> index
        All indecies containing the char at the given string.
*/
vector<unsigned int> str::indecies(const string & str, char c) {
    vector<unsigned int> result;

    for(unsigned int i = 0; i < str.size(); ++i)
        if(str[i] == c) result.push_back(i);

    return result;
}


/*
Finds all indecies of all occurences of a given substr.

@args
    const string & str
        String to be used for.
    const string & substr
        String to be searched for.
    bool exact
        Default: false. Means that if set to true, no overlaping indecies will be included 
        in the result.

@return
    vector<unsigned int> index
        All indecies containing the char at the given string.
*/
vector<unsigned int> str::indecies(const string & str, const string & substr, bool exact) {
    vector<unsigned int> result;

    unsigned int step = exact && substr.size() ? substr.size() : 1;
    for(unsigned int i = 0; i < str.size() - substr.size(); i += step)
        if(str.substr(i, substr.size()) == substr) result.push_back(i);

    return result;
}


/*
Checks if a given char is contained in a string between [begin, end) positions.

@args
    const string & str
        String to be checked.
    char c
        Char to be searched for in the given string.
    unsigned int begin
        Index from which the search to begin.
    unsigned int end
        Index to which search to end.
@return
    true, or false
*/
bool str::between(const string & str, char c, unsigned int begin, unsigned int end) {
    if(begin >= str.size() || end <= 0 || begin >= end)
        throw "Index out of range";

    for(unsigned int i = begin; i < end; i++)
        if(str[i] == c)
            return true;

    return false;
}

/*
Checks if a given string is contained in a string between [begin, end) positions.

@args
    const string & str
        String to be checked.
    const string & substr
        String to be searched for.
    unsigned int begin
        Index from which the search to begin.
    unsigned int end
        Index to which search to end.
@return
    true, or false
*/
bool str::between(const string & str, const string & substr, unsigned int begin, unsigned int end) {
    if(begin >= str.size() || end <= 0 || begin >= end)
        throw "Index out of range";

    if(end > str.size()) end = str.size();

    for(unsigned int i = begin; i < end; i++)
        if(str.substr(i, substr.size()) == substr)
            return true;

    return false;
}

/*
Checks if a given char is contained in the string.

@args
    const string & str
        String which will be checked.
    char c
        Char which will be checked.

@return
    true, or false
*/
bool str::in(const string & str, char c) {
    unsigned int i = 0, j = str.size() - 1;
    while(i <= j) {
        if(str[i++] == c || str[j--] == c)
            return true;
    }

    return false;
}

/*
Checks if a given string is contained in the givne string.

@args
    const string & str
        String which will be checked.
    const string & other
        String which will be checked.

@return
    true, or false
*/
bool str::in(const string & str, const string & other) {
    unsigned int i = 0, j = str.size() - other.size();
    while(i <= j) {
        if(str.substr(i, other.size()) == other || str.substr(j, other.size()) == other)
            return true;
        ++i;
        --j;
    }

    return false;
}