#ifndef STRING_H
#define STRING_H

#include <vector>
#include <map>
#include <string>

using std::vector;
using std::map;
using std::string;

namespace str {
    vector<string> split(const string &, char = ' ');
    vector<string> split(const string &, const string &);
    string strip(const string &, char = ' ', bool = true);
    string strip(const string &, const string &);
    string lstrip(const string&, char = ' ', bool = true);
    string lstrip(const string&, const string &);
    string rstrip(const string&, char = ' ', bool = true);
    string rstrip(const string&, const string &);
    string repeat(const string&, unsigned int, char = ' ', bool = false);
    string repeat(const string&, unsigned int, const string &);
    string normalize(const string&);
    string unique(const string&);

    unsigned int count(const string&, char);
    unsigned int count(const string&, const string&);

    long int occurence(const string&, char, long int);
    long int occurence(const string&, const string&, long int);

    vector<unsigned int> indecies(const string&, char);
    vector<unsigned int> indecies(const string&, const string&, bool = false);

    bool between(const string &, char, unsigned int, unsigned int);
    bool between(const string &, const string&, unsigned int, unsigned int);
    bool in(const string &, char);
    bool in(const string &, const string &);
}

#endif