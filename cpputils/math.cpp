#include "math.h"

using namespace math;

map<char, short> precedence = {
        { '*', 1 },
        { '/', 1 },
        { '\\', 1 },
        { '+', 0 },
        { '-', 0 },
        { '(', 2 }
};

stack<char> op;
stack<string> numbers;
string number;

/*
Computes a string expression, and keeps the result in an int object.

@args
    const string & expr
        Math expr, given as string.
    int & result
        Assigns the result to that arg.
@throws
    An error, if you try to divide by 0.
*/
void math::compute(const string & expr, int & result) {

    for(unsigned int i = 0; i < expr.size(); i++) {
        switch(expr[i]) {
            case '(':
                op.push(expr[i]);
                break;
            case '*':
            case '\\':
            case '/':
            case '+':
            case '-':
                if(op.size() && op.top() != '(' && precedence[op.top()] >= precedence[expr[i]])
                    _operation(numbers, op, "int");

                op.push(expr[i]);
                break;
            case ')':
                while(op.top() != '(')
                    _operation(numbers, op, "int");
                op.pop();
                break;
            default:
                if(expr[i] == ' ')
                    continue;

                while(expr[i] >= '0' && expr[i] <= '9')
                    number.push_back(expr[i++]);

                numbers.push(number);
                number.clear();
                i -= 1;
        }
    }

    while(op.size())
        _operation(numbers, op, "int");

    result = stoi(numbers.top());
    _clear();
}

/*
    Help method.
    ! Don't use it !
*/
void math::_operation(stack<string> & numbers, stack<char> & op, string type) {
    int ilhs, irhs;
    if(type == "int") {
        irhs = std::stoi(numbers.top());
        numbers.pop();
        ilhs = std::stoi(numbers.top());
        numbers.pop();
    }

    char operation = op.top();
    op.pop();
    switch(operation) {
        case '+':
            if(type == "int")
                numbers.push(std::to_string(ilhs + irhs));
            break;
        case '-':
            if(type == "int")
                numbers.push(std::to_string(ilhs - irhs));
            break;
        case '*':
            if(type == "int")
                numbers.push(std::to_string(ilhs * irhs));
            break;
        case '\\':
        case '/':
        if(type == "int") {
            if(irhs != 0)
                numbers.push(std::to_string(ilhs / irhs));
            else
                throw "Division by 0!";
        }
        break;
    }
}


/*
    Help method.
    ! Don't use it !
*/
void math::_clear() {
    number.clear();
    
    while(numbers.size()) numbers.pop();
    while(op.size()) op.pop();
}