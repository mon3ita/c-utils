#ifndef MATH_H
#define MATH_H

#include <map>
#include <stack>
#include <string>

using std::map;
using std::stack;
using std::string;

namespace math {
    void compute(const string &, int &);
    void compute(const string&, float &);
    void compute(const string &, double &);
    void compute(const string &, unsigned int &);

    void _operation(stack<string> &, stack<char> &, string );
    void _clear();
}

#endif